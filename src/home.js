var express = require('express');
var router = express.Router();
const sqlite3 = require('sqlite3');

// Supporting Scripts

router.get('/', function (req, res){
  res.writeHead(200,
    {
      'Content-Type': 'text/event-stream',
      'Cache-Control': 'no-cache',
      'Connection': 'keep-alive'
    }
  );
  countdown (res, 10);
});

function countdown (res, count) {
  res.write("data: " + count + "\n\n");
  if (count) {
    setTimeout(() => countdown(res, count - 1), 1000);
  } else {
    res.end();
  }
}

router.get('/test', function (req, res){
  res.write ("<html>");
  res.write ("<p> Devices </p></br>");
  let db = new sqlite3.Database('./db/fermenter.db');
  let deviceList = "<ul>";
  db.serialize(() => {
    db.each ('SELECT name deviceName FROM devices',
             [],
             (err, row) => {
               if (err) {
                 console.log (err);
               } else {
                 console.log (row.deviceName);
                 deviceList += "<li>";
                 deviceList += row.deviceName;
                 deviceList += "</li>";
               }
             }
    );
  });

  db.close((err) => {
    if (err) {
      return console.error (err.message);
    }
    deviceList += "</ul>"
    res.write(deviceList);
    res.write("</html>");
    res.end();
  });

});

module.exports = router;
