// Express App
var express = require('express');
var app = express();

// Require Routes
var home = require('./home.js');
var register = require('./register.js');
var temperature = require('./temperature.js');

// Establish Server info
var config = require('./config.js');
const hostname = config.hostname;
const port = config.port;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Use Routes for pages
app.use('/', express.static('public'));
app.use('/register', register);
app.use('/temperature', temperature);
app.use('/countdown', home);
// Start Express App
app.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});
