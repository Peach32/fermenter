var express = require('express');
var router = express.Router();
const sqlite3 = require('sqlite3');

// Supporting Scripts

router.get('/', function (req, res){
  res.writeHead(200,
    {
      'Content-Type': 'text/event-stream',
      'Cache-Control': 'no-cache',
      'Connection': 'keep-alive'
    }
  );
  getTemp(res);
});

function getTemp(res) {
  let retVal = {"id": "", "tempF":""};
  let db = new sqlite3.Database('./db/fermenter.db');

  db.serialize(() => {
    db.each ('SELECT name deviceName, temperature_f tempF FROM temperature',
             [],
             (err, row) => {
               if (err) {
                 console.log (err);
               } else {
                 console.log (row);
                 retVal.id    = row.deviceName;
                 retVal.tempF = row.tempF;
                 res.write("data: " + JSON.stringify(retVal) + "\n");
               }
             }
    );
  });

  db.close((err) => {
    if (err) {
      return console.error (err.message);
    }
//    console.log (retVal);
  //  res.json(retVal);
    res.write("\n");
  });
  // wait a second, then try again
  setTimeout(() => getTemp(res), 5000);
}

router.post('/create', function (req, res){
  console.log ("Create Temperature: " + JSON.stringify(req.body));
  let retVal = { "device_id" : req.body.id };

  let db = new sqlite3.Database('./db/fermenter.db');

  db.serialize(() => {
    db.run('CREATE TABLE IF NOT EXISTS temperature (name TEXT UNIQUE, temperature_f INTEGER)')
      .run('INSERT INTO temperature (name, temperature_f) VALUES (?, ?)',
           [req.body.id, req.body.temperature_f],
           function(err) {
             if (err) {
               retVal.status = "failure";
               retVal.error = err.message;
             } else {
               retVal.status = "success";
               retVal.error = "none";
             }
           }
      );
  });

  db.close((err) => {
    if (err) {
      return console.error (err.message);
    }
    console.log (retVal);
    res.json(retVal);
  });
});

router.post('/read', function (req, res){
  console.log ("Read Temperature: " + JSON.stringify(req.body));
  let retVal = { "device_id" : req.body.id };

  let db = new sqlite3.Database('./db/fermenter.db');

  db.serialize(() => {
    db.each ('SELECT name deviceName, temperature_f tempF FROM temperature',
             [],
             (err, row) => {
               if (err) {
                 console.log (err);
               } else {
                 console.log (row);
                 retVal += row.deviceName;
                 retVal += row.tempF;
               }
             }
    );
  });

  db.close((err) => {
    if (err) {
      return console.error (err.message);
    }
    console.log (retVal);
    res.json(retVal);
  });

});

router.post('/update', function (req, res){
  console.log ("Update Temperature: " + JSON.stringify(req.body));
});

router.post ('/destroy', function (req, res){
  console.log ("Destroy Temperature: " + JSON.stringify(req.body));
});

module.exports = router;
