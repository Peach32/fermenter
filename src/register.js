var express = require('express');
var router = express.Router();
const sqlite3 = require('sqlite3');

// Supporting Scripts

router.post('/', function (req, res){
  console.log ("Register: " + JSON.stringify(req.body));
  let retVal = { "device_id" : req.body.id };

  let db = new sqlite3.Database('./db/fermenter.db');

  db.serialize(() => {
    db.run('CREATE TABLE IF NOT EXISTS devices (name TEXT UNIQUE)')
      .run('INSERT INTO devices (name) VALUES (?)',
           [req.body.id],
           function(err) {
             if (err) {
               retVal.status = "failure";
               retVal.error = err.message;
             } else {
               retVal.status = "success";
               retVal.error = "none";
             }
           }
      );
  });

  db.close((err) => {
    if (err) {
      return console.error (err.message);
    }
    console.log (retVal);
    res.json(retVal);
  });

});

module.exports = router;
